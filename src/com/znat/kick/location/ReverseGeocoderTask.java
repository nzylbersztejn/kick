/*
 * Copyright (C) 2014 Nathan Zylbersztejn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.znat.kick.location;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;

import com.znat.kick.location.LatLng;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class ReverseGeocoderTask extends AsyncTask<LatLng, Void, Object> {
    private Geocoder geocoder;
    private OnReverseGeocoderResultListener resultListener;

    public ReverseGeocoderTask(Context context, OnReverseGeocoderResultListener l) {
        Locale locale = Locale.getDefault();
        geocoder = new Geocoder(context, locale);
        resultListener = l;
    }

    @Override
    protected Object doInBackground(LatLng... params) {
        LatLng latLng = params[0];
        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocation(latLng.latitude,
                    latLng.longitude, 1);
        } catch (IOException e) {
            return e;
        }
        if (addresses != null && addresses.size() > 0)
            return addresses.get(0);
        else
            return null;
    }

    @Override
    protected void onPostExecute(Object result) {
        if (result instanceof IOException) {
            resultListener.onError((IOException) result);
        } else if (result == null){
            resultListener.onError(null);
        } else {
            resultListener.onResult((Address) result);
        }
    }

    public interface OnReverseGeocoderResultListener {
        public void onResult(Address address);

        public void onError(IOException e);
    }
}
