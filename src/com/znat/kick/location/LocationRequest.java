/*
 * Copyright (C) 2014 Nathan Zylbersztejn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.znat.kick.location;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

/**
 * 1) Listen to network location
 * 2) if location found and accuracy < minAccuracy, return it
 * 3) else checks for a gps fix
 * 4) return location with best accuracy
 */

public class LocationRequest {
    private int minAccuracy = 300; // in meters
    private int gpsTimeOut = 30000; // in millis

    public enum Provider{GPS,NETWORK,BOTH,UNDEFINED};

    public static final int ERROR_NO_PROVIDER_ENABLED = 5;
    public static final int WARNING_GPS_DISABLED = 6;
    public static final int WARNING_NETWORK_DISABLED = 7;
    public static final int LOCATION_UPDATE = 0;
    public static final int GPS_UPDATE = 0;
    public static final int ERROR_LOCATION_TIME_OUT = 4;
    public static final int NETWORK_UPDATE = 1;
    public static String TAG = LocationRequest.class.getSimpleName();
    private Handler handler;

    private LocationManager lm;
    private OnLocationResult locationResult;
    boolean gpsEnabled = false;
    boolean networkEnabled = false;
    private Provider mRequiredProvider = Provider.UNDEFINED;

    private static boolean isRequesting;


    private LocationRequest(){
    }

    /**
     * Initialize a location request
     * @return
     */
    public static LocationRequest create(){
        return new LocationRequest();
    }

    /**
     * Set the minimum accuracy required
     * @param minAccuracyInMeters
     * @return
     */
    public LocationRequest withAccuracy(int minAccuracyInMeters){
        this.minAccuracy = minAccuracyInMeters;
        return this;
    }

    /**
     * Set the timeout
     * @param timeOutInMillis
     * @return
     */
    public LocationRequest withTimeOut(int timeOutInMillis){
        this.gpsTimeOut = timeOutInMillis;
        return this;
    }

    /**
     * Set the interface instance used as callback
     * @param l
     * @return
     */
    public LocationRequest withCallBack(OnLocationResult l){
        this.locationResult = l;
        return this;
    }

    /**
     * Set a required provider (GPS,NETWORK,BOTH). If the provider is not enabled it will return an error
     * @param provider
     * @return
     */
    public LocationRequest requires(Provider provider){
        this.mRequiredProvider = provider;
        return this;
    }

    /**
     * Fire the request
     * @param context
     * @return
     */
    public LocationRequest start(Context context){
        if (locationResult == null)
            throw new IllegalStateException("Must call withCallBack(OnLocationResult) before starting a request");
        init();
        getLocation(context);
        return this;
    }


    private void init() {
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
            Location location = null;
            switch (msg.what) {
                case LOCATION_UPDATE:

                    switch (msg.arg1) {
                        case GPS_UPDATE:
                            Location gpsLocation = (Location) msg.obj;
                            if (location == null || gpsLocation.getAccuracy() < location.getAccuracy()) {
                                location = gpsLocation;
                            }
                            break;

                        case NETWORK_UPDATE:
                            if (((Location) msg.obj).getAccuracy() < minAccuracy){
                                location = (Location) msg.obj;
                            }
                            break;
                    }

                    if (location != null){
                        removeMessages(ERROR_LOCATION_TIME_OUT);

                        locationResult.onSuccess(location);
                        lm.removeUpdates(locationListenerGps);
                        lm.removeUpdates(locationListenerNetwork);
                        isRequesting = false;
                    }
                    break;

                case ERROR_LOCATION_TIME_OUT:
                    locationResult.onFailure(ERROR_LOCATION_TIME_OUT, getLastKnownLocation());
                    isRequesting = false;
                    break;

                case ERROR_NO_PROVIDER_ENABLED:
                    locationResult.onFailure(ERROR_NO_PROVIDER_ENABLED, getLastKnownLocation());
                    isRequesting = false;
                    break;

                case WARNING_GPS_DISABLED:
                    if (mRequiredProvider == Provider.GPS || mRequiredProvider == Provider.BOTH) {
                        locationResult.onFailure(WARNING_GPS_DISABLED, getLastKnownLocation());
                        isRequesting = false;
                    }
                    else
                        locationResult.onWarning(WARNING_GPS_DISABLED);
                    break;

                case WARNING_NETWORK_DISABLED:
                    if (mRequiredProvider == Provider.NETWORK || mRequiredProvider == Provider.BOTH) {
                        locationResult.onFailure(WARNING_NETWORK_DISABLED, getLastKnownLocation());
                        isRequesting = false;
                    }
                    else
                        locationResult.onWarning(WARNING_NETWORK_DISABLED);
                    break;
                }
            }
        };
    }

    /**
     * Cancel a request
     */
    public void cancel() {

        lm.removeUpdates(locationListenerGps);
        lm.removeUpdates(locationListenerNetwork);
        handler.removeMessages(ERROR_LOCATION_TIME_OUT);
        isRequesting = false;
    }

    /**
     * Returns true if there is an ongoing request;
     * @return
     */
    public static boolean isRequesting(){
        return  isRequesting;
    }

    /**
     * Get the min accuracy required for this request
     * @return
     */
    public int getMinAccuracy() {
        return minAccuracy;
    }

    /**
     * Get the the current timeout for this request
     * @return
     */
    public int getGpsTimeOut() {
        return gpsTimeOut;
    }

    private boolean getLocation(Context context) {
        isRequesting = true;
        boolean networkEnabled, gpsEnabled;
        if (lm == null)
            lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        // Try listening to network
        networkEnabled = listenToNetwork();
        gpsEnabled = listenToGps();

        if (!networkEnabled && !gpsEnabled){
            Message msg = Message.obtain();
            msg.what = ERROR_NO_PROVIDER_ENABLED;
            handler.sendMessage(msg);
            return false;
        }

        if (!gpsEnabled){
            Message msg = Message.obtain();
            msg.what = WARNING_GPS_DISABLED;
            handler.sendMessage(msg);
        }

        if (!networkEnabled){
            Message msg = Message.obtain();
            msg.what = WARNING_NETWORK_DISABLED;
            handler.sendMessage(msg);
        }

        // This message will be sent to the handler in TIME_OUT millis.
        Message msg = Message.obtain();
        msg.what = ERROR_LOCATION_TIME_OUT;
        handler.sendMessageDelayed(msg, gpsTimeOut);
        return true;
    }

    private boolean listenToNetwork() {
        // exceptions will be thrown if provider is not permitted.
        try {
            networkEnabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
            return false;
        }

        lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListenerNetwork);
        return networkEnabled;
    }

    private boolean listenToGps() {
        lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListenerGps);
        try {
            gpsEnabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
            return false;
        }

        lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListenerNetwork);
        return gpsEnabled;
    }

    private LocationListener locationListenerNetwork = new LocationListener() {
        public void onLocationChanged(Location location) {
            Message msg = Message.obtain();
            msg.what = LOCATION_UPDATE;
            msg.arg1 = NETWORK_UPDATE;
            msg.obj = location;
            handler.sendMessage(msg);
            lm.removeUpdates(this);
        }

        public void onProviderDisabled(String provider) {}
        public void onProviderEnabled(String provider) {}
        public void onStatusChanged(String provider, int status, Bundle extras) {}
    };

    private LocationListener locationListenerGps = new LocationListener() {
        public void onLocationChanged(Location location) {
            Message msg = Message.obtain();
            msg.what = LOCATION_UPDATE;
            msg.arg1 = NETWORK_UPDATE;
            msg.obj = location;
            handler.sendMessage(msg);
        }

        public void onProviderDisabled(String provider) {}
        public void onProviderEnabled(String provider) {}
        public void onStatusChanged(String provider, int status, Bundle extras) {}
    };

    public interface OnLocationResult {
        public void onSuccess(Location location);
        public void onWarning(int warningCode);
        public void onFailure(int errorCode, Location lastKnown);
    }

    private Location getLastKnownLocation() {
        Location locationGps = null, locationNtw = null, locationToReturn = null;
        lm.removeUpdates(locationListenerGps);
        lm.removeUpdates(locationListenerNetwork);

        if (gpsEnabled)
            locationGps = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if (networkEnabled)
            locationNtw = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

        // if there are both values use the latest one
        if (locationGps != null && locationNtw != null)
            if (locationGps.getTime() > locationNtw.getTime())
                locationToReturn = locationGps;
            else
                locationToReturn = locationNtw;

        return locationToReturn;
    }
}
