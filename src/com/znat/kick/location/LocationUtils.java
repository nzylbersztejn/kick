package com.znat.kick.location;

import android.content.Context;
import android.location.LocationManager;

/*
 * Copyright (C) 2014 Nathan Zylbersztejn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class LocationUtils {
    public static boolean isAnyLocationServiceEnabled(Context context){
       return (isNetworkLocationProviderEnabled(context) || isGPSLocationProviderEnabled(context));
    }

    public static boolean isNetworkLocationProviderEnabled(Context context){
        LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        return lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    public static boolean isGPSLocationProviderEnabled(Context context){
        LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        return lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

}
