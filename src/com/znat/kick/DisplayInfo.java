/*
 * Copyright (C) 2014 Nathan Zylbersztejn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.znat.kick;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Point;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Display;
import android.view.Surface;
import android.view.WindowManager;

public class DisplayInfo {
	public static final String TAG = DisplayInfo.class.getSimpleName();
	private static int screenDensity, screenWidth, screenHeight, actionBarHeight, deviceType;
	public static final int TYPE_PHONE = 0;
	public static final int TYPE_TABLET = 1;
	public enum OrientationMode{PORTRAIT,LANDSCAPE};
	

	@SuppressWarnings("deprecation")
	@SuppressLint("NewApi")
	public static void init(Context context, OrientationMode mode) {
		WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		DisplayMetrics metrics = new DisplayMetrics();
		display.getMetrics(metrics);
		screenDensity = metrics.densityDpi;
		if (Build.VERSION.SDK_INT >= 13) {
			Point displaySize = new Point();
			display.getSize(displaySize);
			screenWidth = mode == OrientationMode.LANDSCAPE ? Math.max(displaySize.x, displaySize.y) : Math.min(displaySize.x, displaySize.y);
			screenHeight = mode == OrientationMode.LANDSCAPE ? Math.min(displaySize.x, displaySize.y) : Math.max(displaySize.x, displaySize.y);
		} else {
			screenWidth = mode == OrientationMode.LANDSCAPE ? Math.max(display.getWidth(), display.getHeight()) : Math.min(display.getWidth(), display.getHeight());
			screenHeight = mode == OrientationMode.LANDSCAPE ? Math.min(display.getWidth(), display.getHeight()) : Math.max(display.getWidth(), display.getHeight());
		}
		
		//Action bar
		TypedValue typeValue = new TypedValue();
		context.getTheme().resolveAttribute(android.R.attr.actionBarSize, typeValue, true);
		actionBarHeight = TypedValue.complexToDimensionPixelSize(typeValue.data,context.getResources().getDisplayMetrics());
		
		//Device type
		int orientation = context.getResources().getConfiguration().orientation;
		int rotation = display.getRotation();
		if (orientation == Configuration.ORIENTATION_PORTRAIT && rotation == Surface.ROTATION_0 
				|| orientation == Configuration.ORIENTATION_PORTRAIT && rotation == Surface.ROTATION_180
				|| orientation == Configuration.ORIENTATION_LANDSCAPE && rotation == Surface.ROTATION_90
				|| orientation == Configuration.ORIENTATION_LANDSCAPE && rotation == Surface.ROTATION_270){
			deviceType = TYPE_PHONE;
//			Log.d(TAG, "PHONE");
//			Toast.makeText(context, "PHONE", Toast.LENGTH_SHORT);
		}
		else {
			deviceType = TYPE_TABLET;
//			Log.d(TAG, "TABLET");
//			Toast.makeText(context, "TABLET", Toast.LENGTH_SHORT);
		}

	}

	public static int getScreenDensity() {
		return screenDensity;
	}

	public static float getScreenDensityRatio() {
		return screenDensity / 160f;
	}

	public static int getScreenWidth() {
		return screenWidth;
	}
	
	public static int getScreenWidthInDp() {
		return (int)(screenWidth / getScreenDensityRatio());
	}
	
	public static int getScreenHeight() {
		return screenHeight;
	}

	public static int getScreenHeightInDp() {
		return (int)(screenHeight / getScreenDensityRatio());
	}

	public static int dpToPx(int dp) {
		return (int) (dp * getScreenDensityRatio());
	}	
	public static int pxToDp(int px) {
		return (int) (px / getScreenDensityRatio());
	}	
	public static int getActionBarHeight(){
		return actionBarHeight;
	}

    /**
     * Returns the status bar height (top of the screen)
     * @param context
     * @return
     */
    public static int getStatusBarHeight(Activity context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }
    public static int getDeviceType(){
		return deviceType;
	}
	
	public static int getOrientation(Context context){
		return context.getResources().getConfiguration().orientation;
	}
}
