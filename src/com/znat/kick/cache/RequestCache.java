/*
 * Copyright (C) 2014 Nathan Zylbersztejn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.znat.kick.cache;

import android.content.Context;
import android.os.AsyncTask;

import com.znat.kick.Log;
import com.znat.kick.Preferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;


public class RequestCache {

    private static final String PREF_CACHE_LIST = "cache_list_987o98";
    private static final String PREF_CACHE_LIST_SEPARATOR = "#!@";
    private static final String PREF_CACHE_PREFIX = "kick_cache_98ypbN_";
    private static final String PREF_TIMESTAMP = "timestamp";
    private static final String PREF_LIFE_EXPECTANCY = "expectancy";
    private static final String PREF_DATA = "cache_object_data";
    private static AsyncTask<Void,Void,JSONObject> mGetFromCacheTask;

    public enum CacheMode {
        /** Will return the object only if it has not expired. Expired items will be deleted */
        DEFAULT,
        /** Will return the object even if it is expired. Expired items will be deleted */
        FORCE,
        /** Will return the object even if it is expired and renew it's life expectancy from now */
        RENEW;
    }

    /**
     * Put object in cache
     * @param context
     * @param key
     * @param value
     * @param lifeExpectancy in millis
     */
    public static void put(Context context, String key, String value, long lifeExpectancy){
        Log.d("RequestCache","put:"+value);
        try {
            putObjectInPrefs(context, key, new JSONObject(key), lifeExpectancy);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Get object from cache
     * @param context
     * @param key
     * @param cacheMode
     * @return
     */
	public static JSONObject get(Context context, String key, CacheMode cacheMode){
        Log.d("RequestCache","get"+key);
        JSONObject obj = getObjectFromPrefs(context,key, cacheMode);
        if (obj != null)
            Log.d("RequestCache","Cache hit for:"+key);
        return obj;
	}

    /**
     * Removes expired cache entries
     * @param context
     */
    public static void clean(Context context){
        final Set<String> keys = loadKeys(context);
        for (String key: keys){
            try {
                JSONObject cacheObject = new JSONObject(Preferences.getString(context,key));
                long expiry = cacheObject.getLong(PREF_TIMESTAMP) + cacheObject.getLong(PREF_LIFE_EXPECTANCY);
                if (System.currentTimeMillis() > expiry){
                    removeObjectFromPrefs(context,key);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private static void putObjectInPrefs(final Context context, final String key, final JSONObject object, final long lifeExpectancy){
        new AsyncTask<Void,Void,Void>(){
            @Override
            protected Void doInBackground(Void... params) {
                synchronized (this) {
                    String defkey = createKey(key);
                    JSONObject cacheObject = new JSONObject();
                    try {
                        cacheObject.put(PREF_TIMESTAMP, System.currentTimeMillis());
                        cacheObject.put(PREF_LIFE_EXPECTANCY, lifeExpectancy);
                        cacheObject.put(PREF_DATA, object);
                        Preferences.setString(context, defkey, cacheObject.toString());
                        addKeyToIndex(context, defkey);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    return null;
                }
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private static synchronized JSONObject getObjectFromPrefs(Context context, String key, CacheMode cacheMode){
        key = createKey(key);
        String jsonString = Preferences.getString(context,key);
        JSONObject objectToReturn = null;

        if (jsonString != null && !jsonString.isEmpty()) {
            try {
                JSONObject cacheObject = new JSONObject(jsonString);
                objectToReturn = cacheObject.getJSONObject(PREF_DATA);
                switch (cacheMode) {
                    case FORCE:
                        objectToReturn = cacheObject.getJSONObject(PREF_DATA);
                        break;
                    case DEFAULT:
                        if (System.currentTimeMillis() > cacheObject.getLong(PREF_TIMESTAMP) + cacheObject.getLong(PREF_LIFE_EXPECTANCY)) {
                            removeObjectFromPrefs(context, key);
                            objectToReturn = null;
                        }
                        break;
                    case RENEW:
                        putObjectInPrefs(context, key, cacheObject.getJSONObject(PREF_DATA), cacheObject.getLong(PREF_LIFE_EXPECTANCY));
                        break;
                }

            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        return objectToReturn;
    }

    private static synchronized void removeObjectFromPrefs(Context context, String key){
        Preferences.remove(context,key);
        removeKeyFromIndex(context,key);
    }

    private static String createKey(String key){
        return PREF_CACHE_PREFIX + key;
    }

    private static synchronized void addKeyToIndex(Context context, String key) {
        Set<String> keysSet = loadKeys(context);
        keysSet.add(key);
        saveKeys(context, keysSet);
    }

    private static synchronized void removeKeyFromIndex(Context context, String key) {
        Set<String> keysSet = loadKeys(context);
        keysSet.remove(key);
        saveKeys(context, keysSet);
    }


    private static Set<String> loadKeys(Context context){
        Set<String> keysSet;
        String[] keys = null;
        final String listString = Preferences.getString(context, PREF_CACHE_LIST);
        if (listString != null)
            keys = listString.split(PREF_CACHE_LIST_SEPARATOR);
        if (keys != null && keys.length > 0){
            keysSet = new HashSet<String>(Arrays.asList(keys));
        }else{
            keysSet = new HashSet<String>();
        }
        return keysSet;
    }

    private static void saveKeys(Context context, Set<String> keysSet){
        StringBuilder sb = new StringBuilder();
        int count = 0;

        for (String s: keysSet){
            sb.append(s);
            if (count++ < keysSet.size() -1){
                sb.append(PREF_CACHE_LIST_SEPARATOR);
            }
        }
        Preferences.setString(context, PREF_CACHE_LIST, sb.toString());
    }
}
