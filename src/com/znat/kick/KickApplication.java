/*
 * Copyright (C) 2014 Nathan Zylbersztejn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.znat.kick;

import android.app.Application;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

public abstract class KickApplication extends Application {

    /**
     * Defines a debug flag that may be used accross the application
     * @return
     */
    public abstract boolean isDebug();

    public abstract DisplayInfo.OrientationMode getDefaultOrientationMode();

    private static int versionCode = 0;
    private static String versionName = "?";

    /** We keep a static reference to the Application object */
    private static KickApplication mThisApplication;

    @Override
    public void onCreate() {
        mThisApplication = this;
        DisplayInfo.init(this, getDefaultOrientationMode());

        // Version info
        try {
            PackageInfo pinfo = getPackageManager().getPackageInfo(getPackageName(), 0);

            versionCode = pinfo.versionCode;
            versionName = pinfo.versionName;

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns a reference to the Application object
     * @return
     */
    public static KickApplication get(){
        return mThisApplication;
    }

    public static int getVersionCode() {
        return versionCode;
    }

    public static String getVersionName() {
        return versionName;
    }
}
