/*
 * Copyright (C) 2014 Nathan Zylbersztejn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.znat.kick.network;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.znat.kick.KickApplication;

import org.json.JSONArray;

import java.util.HashMap;
import java.util.Map;


public class SimpleJSONArrayRequest extends JsonArrayRequest {
	private static RequestQueue queue;

	public SimpleJSONArrayRequest(Context context, int method, String url, String body, final SimpleJSONArrayRequestListener l) {
		super(preprocess(context, url), method, body, new Response.Listener<JSONArray>() {

			@Override
			public void onResponse(JSONArray response) {
				if (l != null)
					l.onSuccess(response);
			}
		}, new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				if (l != null)
					l.onError(error);
			}
		});

		getRequestQueue(context).add(this);
	}
	
	public void addHeaders(Map<String, String> headers){}
	
	@Override
	public Map<String, String> getHeaders() throws AuthFailureError {
		Map<String, String> headers = new HashMap<String, String>();
		addHeaders(headers);
		return headers;
	}
	
	private static String preprocess(Context context,String url){
		 if (KickApplication.get().isDebug())
			 getRequestQueue(context).getCache().remove(url);
		 return url;
	}

	public interface SimpleJSONArrayRequestListener {
		public void onSuccess(JSONArray response);
		public void onError(Exception exception);
	}

	public static RequestQueue getRequestQueue(Context context) {
		if (queue == null)
			queue = Volley.newRequestQueue(context);

		return queue;
	}


}
