/*
 * Copyright (C) 2014 Nathan Zylbersztejn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.znat.kick.network;

import android.content.Context;
import android.os.AsyncTask;

import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.znat.kick.KickApplication;
import com.znat.kick.Log;
import com.znat.kick.cache.RequestCache;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class SimpleJSONRequest extends JsonObjectRequest {
	private static RequestQueue queue;
    private final SimpleJSONRequestListener mListener;
    private final long mCacheLifeExpectancy;
    private final String mUrl;

    /**
     * Create a new request
     * @param context
     * @param method
     * @param url
     * @param l
     */
    public SimpleJSONRequest(final Context context, int method, final String url, final SimpleJSONRequestListener l){
        this(context,method,url,l,0);
    }

    /**
     * Create a new request
     * @param context
     * @param method
     * @param url
     * @param l
     * @param cacheLifeExpectancy how long (in millis) should the response be kept in cache. The cache is persistent
     */
	public SimpleJSONRequest(final Context context, int method, final String url, final SimpleJSONRequestListener l, final long cacheLifeExpectancy) {
		super(method, preprocess(context,url), null, new Response.Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject response) {
                if (cacheLifeExpectancy > 0){
                    RequestCache.put(context,url,response.toString(),cacheLifeExpectancy);
                }

				l.onSuccess(response);
			}
		}, new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				l.onError(error);
			}
		});
        mListener = l;
        mCacheLifeExpectancy = cacheLifeExpectancy;
        mUrl = url;
        getRequestQueue(context);
	}

    /**
     * Executes the query
     * @param context
     */
    public void execute(Context context){
        execute(context, RequestCache.CacheMode.DEFAULT);
    }

    /**
     * Executes the query
     * @param context
     * @param cacheMode how to deal with cache
     */
    public void execute(final Context context, final RequestCache.CacheMode cacheMode){
        if (mCacheLifeExpectancy > 0){
            // I/O operation might take time so it's asynchronous
            new AsyncTask<Void,Void,JSONObject>(){

                @Override
                protected JSONObject doInBackground(Void... params) {
                    JSONObject cacheObject =  RequestCache.get(context,mUrl, cacheMode);
                    if (cacheObject != null) {
                        mListener.onSuccess(cacheObject);
                    }else{
                        addToQueue();
                    }
                    return cacheObject;
                }

                @Override
                protected void onPostExecute(JSONObject cacheObject) {
                }
            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        }else {
            addToQueue();
        }
    }

    private void addToQueue(){
        queue.add(this);
        Log.d(this,getUrl());
    }
	
	public void addHeaders(Map<String, String> headers){

    }
	
	@Override
	public Map<String, String> getHeaders() throws AuthFailureError {
		Map<String, String> headers = new HashMap<String, String>();
		addHeaders(headers);
		return headers;
	}
	
	private static String preprocess(Context context,String url){
		 if (KickApplication.get().isDebug())
			 getRequestQueue(context).getCache().remove(url);
		 return url;
	}

	public interface SimpleJSONRequestListener {
		public void onSuccess(JSONObject response);
		public void onError(Exception exception);
	}

	public static RequestQueue getRequestQueue(Context context) {
		if (queue == null)
			queue = Volley.newRequestQueue(context);

		return queue;
	}
}
