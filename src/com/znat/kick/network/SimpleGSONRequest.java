/*
 * Copyright (C) 2014 Nathan Zylbersztejn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.znat.kick.network;


import android.app.Activity;
import android.content.Context;

import com.google.gson.Gson;

import org.json.JSONObject;

public class SimpleGSONRequest extends SimpleJSONRequest{

    /**
     *  Create and send a request
     * @param context
     * @param method
     * @param url
     * @param gson
     * @param jsonSubObject
     * @param toClass
     * @param l
     * @param lifeExpectancy
     */
    public SimpleGSONRequest(final Context context, int method, String url, final Gson gson, final String jsonSubObject, final Class<?> toClass, final SimpleGSONRequestListener l, long lifeExpectancy ){
		super(context, method, url, new SimpleJSONRequestListener() {

			@Override
			public void onSuccess(final JSONObject response) {
				final Object result;
				try {
					Gson newGson = gson == null ? new Gson() : gson;
					result = Utils.getObjectfromJson(newGson, response, jsonSubObject, toClass);
					if  (l!=null) {
                       if (context instanceof Activity){
                           Activity act = (Activity) context;
                           act.runOnUiThread(new Runnable() {
                               @Override
                               public void run() {
                                   l.onSuccess(result, response);
                               }
                           });
                       }else{
                           l.onSuccess(result, response);
                       }
                    }
				} catch (Exception e) {
					e.printStackTrace();
					if  (l!=null) l.onError(e);
				}
			}
			
			@Override
			public void onError(final Exception exception) {
				if (l != null)
                    if (context instanceof Activity) {
                        Activity act = (Activity) context;
                        act.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                l.onError(exception);
                            }
                        });
                    } else {
                        l.onError(exception);
                    }
            }
		}, lifeExpectancy);
	}

    /**
     * Create and send a request
     * @param context
     * @param method
     * @param url
     * @param gson
     * @param jsonSubObject
     * @param toClass
     * @param l
     */
    public SimpleGSONRequest(final Context context, int method, String url, final Gson gson, final String jsonSubObject, final Class<?> toClass, final SimpleGSONRequestListener l){
        this(context,method,url,gson,jsonSubObject,toClass,l,0);
    }


	public interface SimpleGSONRequestListener{
		public void onSuccess(Object response, JSONObject jsonObject);
		public void onError(Exception e);
	}
}
