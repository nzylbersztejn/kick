package com.znat.kick.network;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.json.JSONException;
import org.json.JSONObject;

public class Utils {
	public static Object getObjectfromJson(final Gson gson, final JSONObject jsonObject, String jsonSubObject, final Class<?> toClass ) throws JsonSyntaxException, JSONException{
		Object result;
        if (jsonSubObject !=null)
            result = gson.fromJson(jsonObject.getString(jsonSubObject).toString(), toClass);
        else
            result = gson.fromJson(jsonObject.toString(), toClass);
		return result;
	}
	
	public static Object getObjectfromJsonString(final Gson gson,final String jsonString, String jsonSubObject, final Class<?> toClass ) throws JsonSyntaxException, JSONException{
        Object result;
        if (jsonSubObject !=null)
            result = gson.fromJson(new JSONObject(jsonString).getString(jsonSubObject), toClass);
        else
            result = gson.fromJson(jsonString, toClass);
		return result;
	}
}
