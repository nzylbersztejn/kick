package com.znat.kick.network;

import android.content.Context;
import android.graphics.Bitmap;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.Volley;

/**
 * Created by elton on 2014-05-12.
 */
public class SimpleImageRequest extends ImageRequest {

    private static RequestQueue queue;

    public SimpleImageRequest(Context context, String url, Response.Listener<Bitmap> l, int maxWidth, int maxHeight,
                              Bitmap.Config decodeConfig, Response.ErrorListener errorListener) {
        super(url, l, maxWidth, maxHeight, decodeConfig, errorListener);
        getRequestQueue(context).add(this);


    }

    public static RequestQueue getRequestQueue(Context context) {
        if (queue == null)
            queue = Volley.newRequestQueue(context);
        return queue;
    }


}
