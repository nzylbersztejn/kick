/*
 * Copyright (C) 2014 Nathan Zylbersztejn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.znat.kick.network;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class SimpleStringRequest extends StringRequest {
	private static RequestQueue queue;

	public SimpleStringRequest(Context context, int method, String url, final SimpleStringRequestListener l) {
		super(method, preprocess(context,url), new Response.Listener<String>() {

			@Override
			public void onResponse(String response) {
				l.onSuccess(response);
			}
		}, new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				l.onError(error);
			}
		});

		getRequestQueue(context).add(this);
	}
	
	public void addHeaders(Map<String, String> headers){}
	
	@Override
	public Map<String, String> getHeaders() throws AuthFailureError {
		Map<String, String> headers = new HashMap<String, String>();
		addHeaders(headers);
		return headers;
	}
	
	private static String preprocess(Context context,String url){
		 return url;
	}


	public interface SimpleStringRequestListener {
		public void onSuccess(String response);
		public void onError(Exception exception);
	}

	public static RequestQueue getRequestQueue(Context context) {
		if (queue == null)
			queue = Volley.newRequestQueue(context);

		return queue;
	}
}
