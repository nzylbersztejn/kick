/*
 * Copyright (C) 2014 Nathan Zylbersztejn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.znat.kick;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;



public class AlertDialogFragment extends DialogFragment {

	private String mButtonLabel, mMessage, mTitle;
	private int mIconResId, cancelResId, okResId;
	private OnDialogActionListener mOnDialogActionListener;
	private boolean mCancel, mCancelable;
	
	public AlertDialogFragment(){
		mMessage = mTitle = "";
	}
		
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		if (savedInstanceState != null){
			mMessage = savedInstanceState.getString("message");
			mTitle= savedInstanceState.getString("title");
		}
		setCancelable(mCancelable);
//		ContextThemeWrapper context = new ContextThemeWrapper(getActivity(), R.style.Theme_Rfd);
//		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		if (!mTitle.isEmpty())
        builder.setTitle(mTitle);
        builder.setMessage(mMessage);
        String okLabel = okResId > 0 ? getActivity().getString(okResId) : "Ok";
        builder.setPositiveButton(okLabel, new DialogInterface.OnClickListener() {
//        builder.setPositiveButton(okResId==0? R.string.OK:okResId, new DialogInterface.OnClickListener() {      
            @Override
            public void onClick(DialogInterface dialog, int which) {
            	if (mOnDialogActionListener != null)
            		mOnDialogActionListener.onOk();
                dismiss();                  
            }
        });

        String cancelLabel = cancelResId > 0 ? getActivity().getString(cancelResId) : "Cancel";
        if (mCancel)
        	builder.setNegativeButton(cancelLabel, new DialogInterface.OnClickListener() {
        	//        	builder.setNegativeButton(cancelResId == 0 ? R.string.Cancel: cancelResId, new DialogInterface.OnClickListener() {      
                @Override
                public void onClick(DialogInterface dialog, int which) {
                	if (mOnDialogActionListener != null)
                		mOnDialogActionListener.onCancel();
                	dismiss();                  
                }
            });
        
        builder.setOnCancelListener(new OnCancelListener() {
			
			@Override
			public void onCancel(DialogInterface dialog) {
               	if (mOnDialogActionListener != null)
            		mOnDialogActionListener.onCancel();
            	dismiss();                  
			}
		});
        
        return builder.create();
    }	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().setCanceledOnTouchOutside(mCancelable);
		return super.onCreateView(inflater, container, savedInstanceState);
	}
	
	
	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
		savedInstanceState.putString("message",mMessage);
		savedInstanceState.putString("title",mTitle);
		super.onSaveInstanceState(savedInstanceState);
	}
	
	public String getButtonLabel() {
		return mButtonLabel;
	}
	public String getMessage() {
		return mMessage;
	}
	public void setMessage(String mMessage) {
		this.mMessage = mMessage;
	}

	public String getTitle() {
		return mTitle;
	}
	public void setTitle(String mTitle){
		this.mTitle = mTitle;
	}
	public int getIconResId() {
		return mIconResId;
	}
	public void setIconResId(int iconResId) {
		this.mIconResId = iconResId;
	}
	
	public void setOnDialogListener(OnDialogActionListener l){
		this.mOnDialogActionListener = l;
	}
	
	public void setCancel(boolean cancel){
		mCancel = cancel;
	}
	
	public void setCancelLabel(int resId){
		cancelResId = resId;
		setCancel(true);
	}
	public void setOkLabel(int resId){
		okResId = resId;
	}
	
	public boolean isShowing()
	{
	   return getDialog() != null;
	}
	
	public interface OnDialogActionListener{
		public void onOk();
		public void onCancel();
	}
}
