/*
 * Copyright (C) 2014 Nathan Zylbersztejn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.znat.kick;


public class Log {

	
	public static void i(Object object, String message) {
        if (message == null)
            return;

		if (!KickApplication.get().isDebug()) {
			return;
		}
		android.util.Log.i(getTag(object), message);
	}

	public static void v(Object object, String message) {
        if (message == null)
            return;

		if (!KickApplication.get().isDebug()) {
			return;
		}
        android.util.Log.v(getTag(object), message);
	}

	public static void d(Object object, String message) {
        if (message == null)
            return;

		if (!KickApplication.get().isDebug()) {
			return;
		}

		android.util.Log.d(getTag(object), message);
	}

	public static void d(Object object, String message, Throwable e) {
        if (message == null)
            return;

		if (!KickApplication.get().isDebug()) {
			return;
		}

		android.util.Log.d(getTag(object), message, e);
	}

	public static void w(Object object, String message) {
        if (message == null)
            return;

		if (!KickApplication.get().isDebug()) {
			return;
		}

		android.util.Log.w(getTag(object), message);
	}

	public static void w(Object object, String message, Throwable e) {
        if (message == null)
            return;

		if (!KickApplication.get().isDebug()) {
			return;
		}

		android.util.Log.w(getTag(object), message, e);
	}

	public static void e(Object object, String message, Throwable e) {
        if (message == null)
            return;

		if (!KickApplication.get().isDebug()) {
			return;
		}

		android.util.Log.e(getTag(object), message, e);
	}

	public static void e(Object object, String message) {
        if (message == null)
            return;

		if (!KickApplication.get().isDebug()) {
			return;
		}

		android.util.Log.e(getTag(object), message);
	}

    public static void LogStackTrace(Object object){
        StackTraceElement[] stack = Thread.currentThread().getStackTrace();
        for (StackTraceElement element: stack){
            Log.d(object,element.toString());
        }
    }

    private static String getTag(Object object){
        String tag;
        if (object instanceof String){
            tag = (String) object;
        }else {
            tag = object != null ? object.getClass().getSimpleName() : "";
        }
        return tag;
    }
	
}
