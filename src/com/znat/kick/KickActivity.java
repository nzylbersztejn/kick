/*
 * Copyright (C) 2014 Nathan Zylbersztejn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.znat.kick;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nathan on 2014-04-16.
 */
public class KickActivity extends FragmentActivity {

    private List<Runnable> mToRunOnResume = new ArrayList<Runnable>();
    private List<Runnable> mToRunOnCreateOptionsMenu = new ArrayList<Runnable>();


    private boolean mIsAlive;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        KickSession.getInstance(this).notifyOnCreate(this);
        if (KickApplication.get().isDebug()){
            ViewServer.get(this).addWindow(this);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        KickSession.getInstance(this).notifyOnStart(this);
    }

    @Override
    public void onResume() {
        KickSession.getInstance(this).notifyOnResume(this);
        super.onResume();
        mIsAlive = true;
        for (Runnable r: mToRunOnResume){
            r.run();
        }
        mToRunOnResume.clear();
    }

    @Override
    public void onPause() {
        super.onPause();
        KickSession.getInstance(this).notifyOnPause(this);
        mIsAlive = false;
    }

    @Override
    protected void onStop() {
        super.onStop();
        KickSession.getInstance(this).notifyOnStop(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        KickSession.getInstance(this).notifyOnDestroy(this);
        if (KickApplication.get().isDebug()){
            ViewServer.get(this).removeWindow(this);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        for (Runnable r: mToRunOnCreateOptionsMenu){
            r.run();
        }
        mToRunOnCreateOptionsMenu.clear();
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * Adds a runnable to be executed onResume or immediately if onResume has already been called
     * @param r
     */
    public void runNowOrAfterOnResume(Runnable r) {
        if (isAlive())
            r.run();
        else
            mToRunOnResume.add(r);
    }

    /**
     * Adds a runnable to be executed onCreateOptionsMenu or immediately if onResume has already been called
     * @param r
     */
    public void runNowOrAfterOnCreateOptionsMenu(Runnable r){
        if (isAlive())
            r.run();
        else
            mToRunOnCreateOptionsMenu.add(r);
    }



    public boolean isAlive(){
        return mIsAlive;
    }
}
