/*
 * Copyright (C) 2014 Nathan Zylbersztejn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.znat.kick;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;


/**
 * A class containing convenience methods to insert or replace a fragment.
 * @author nathanzylbersztejn
 *
 */
public class FragmentSlider {


    /**
     * Convenience method to replace a fragment
     * @param fm the FragmentManager
     * @param f the replacement fragment
     * @param addToBackstack true if the transaction should be added to the backstack
     * @param containerResId ViewGroup resid where the fragment is located
     */
    public static void replaceFragment(FragmentManager fm,Fragment f, boolean addToBackstack, int containerResId){
		replaceFragment(fm, f, null,addToBackstack, containerResId, 0, 0, 0, 0);
	}

    /**
     * Convenience method to replace a fragment
     * @param fm the FragmentManager
     * @param f the replacement fragment
     * @param tag used as name in the back stack and as a tag for the transaction
     * @param addToBackstack true if the transaction should be added to the backstack
     * @param containerResId ViewGroup resid where the fragment is located
     */
    public static void replaceFragment(FragmentManager fm,Fragment f, String tag, boolean addToBackstack, int containerResId){
        replaceFragment(fm, f, tag, addToBackstack, containerResId, 0, 0,0,0);
    }

    /**
     * Convenience method to replace a fragment
     * @param fm the FragmentManager
     * @param f the replacement fragment
     * @param addToBackstack true if the transaction should be added to the backstack
     * @param containerResId ViewGroup resid where the fragment is located
     * @param animInResId animation
     * @param animOutResId animation
     */
	public static void replaceFragment(FragmentManager fm,Fragment f, boolean addToBackstack, int containerResId, int animInResId, int animOutResId){
        replaceFragment(fm, f, null, addToBackstack, containerResId, animInResId, animOutResId,0,0);
	}

    /**
     * Convenience method to replace a fragment
     * @param fm the FragmentManager
     * @param f the replacement fragment
     * @param tag used as name in the back stack and as a tag for the transaction
     * @param addToBackstack true if the transaction should be added to the backstack
     * @param containerResId ViewGroup resid where the fragment is located
     * @param animInResId animation
     * @param animOutResId animation
     */
    public static void replaceFragment(FragmentManager fm,Fragment f, String tag, boolean addToBackstack, int containerResId, int animInResId, int animOutResId, int popEnter, int popExit){
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        String theTag = tag != null ? tag : ((Object)f).getClass().getName();
        if (addToBackstack)
            fragmentTransaction.addToBackStack(theTag);

        if(animInResId > 0 && animOutResId > 0 && popEnter > 0 && popExit > 0)
            fragmentTransaction.setCustomAnimations(animInResId,animOutResId,popEnter,popExit);
        else if (animInResId > 0 && animOutResId > 0 ) {
            fragmentTransaction.setCustomAnimations(animInResId,animOutResId);
        }

        fragmentTransaction.replace(containerResId, f, theTag);
        fragmentTransaction.commit();
    }

    /**
     * Convenience method to remove a fragment
     * @param fm the FragmentManager
     * @param f the replacement fragment
     * @param animInResId animation
     * @param animOutResId animation
     */
	public static void removeFragment(FragmentManager fm,Fragment f, int animInResId, int animOutResId){
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.setCustomAnimations(animInResId,animOutResId);
        fragmentTransaction.remove(f);
        fragmentTransaction.commit();
	}

}

