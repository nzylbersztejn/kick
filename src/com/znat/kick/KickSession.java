/*
 * Copyright (C) 2014 Nathan Zylbersztejn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.znat.kick;

import android.content.Context;

/**
 * Created by nathan on 2014-04-24.
 */
public class KickSession {
    private static final long DEFAULT_MAX_INTERRUPTION_TIME = 3000;
    static final String PREF_SESSION_MAX_INTERRUPTION_TIME = "session_max_interruption_time_098798279w8ey";
    static final String PREF_LAST_ON_CREATE_TIME = "last_on_create_time_098798279w8ey";
    static final String PREF_LAST_ON_START_TIME = "last_on_start_time_098798279w8ey";
    static final String PREF_LAST_ON_RESUME_TIME = "last_on_resume_time_098798279w8ey";
    static final String PREF_LAST_ON_PAUSE_TIME = "last_on_pause_time_098798279w8ey";
    static final String PREF_LAST_ON_STOP_TIME = "last_on_stop_time_098798279w8ey";
    static final String PREF_LAST_ON_DESTROY_TIME = "last_on_destroy_098798279w8ey";

    private static KickSession instance;

    public static KickSession getInstance(Context context){
        if (instance == null){
            instance = new KickSession(context);
        }
        return instance;
    }

    private KickSession(Context context){
        Preferences.setLong(context, PREF_SESSION_MAX_INTERRUPTION_TIME, DEFAULT_MAX_INTERRUPTION_TIME);
    }

    public void setMaxInterruptionTime(Context context, long maxInterruptionTime){
        Preferences.setLong(context, PREF_SESSION_MAX_INTERRUPTION_TIME, maxInterruptionTime);
    }

    public long getMaxInterruptionTime(Context context){
        return Preferences.getLong(context, PREF_SESSION_MAX_INTERRUPTION_TIME);
    }

    public void notifyOnCreate(Context context){
        Preferences.setLong(context, KickSession.PREF_LAST_ON_CREATE_TIME, System.currentTimeMillis());
    }

    public void notifyOnStart(Context context){
        Preferences.setLong(context, KickSession.PREF_LAST_ON_START_TIME, System.currentTimeMillis());
    }

    public void notifyOnResume(Context context){
        Preferences.setLong(context, KickSession.PREF_LAST_ON_RESUME_TIME, System.currentTimeMillis());
    }

    public void notifyOnPause(Context context){
        Preferences.setLong(context, KickSession.PREF_LAST_ON_PAUSE_TIME, System.currentTimeMillis());
    }

    public void notifyOnStop(Context context){
        Preferences.setLong(context, KickSession.PREF_LAST_ON_STOP_TIME, System.currentTimeMillis());
    }

    public void notifyOnDestroy(Context context){
        Preferences.setLong(context, KickSession.PREF_LAST_ON_DESTROY_TIME, System.currentTimeMillis());
    }

    private static long getLastStopTime(Context context){
        return Preferences.getLong(context, PREF_LAST_ON_STOP_TIME);
    }

    private static long getLastCreateTime(Context context){
        return Preferences.getLong(context, PREF_LAST_ON_CREATE_TIME);
    }

    public boolean isNew(Context context){
        return getLastStopTime(context) != 0
                && System.currentTimeMillis() - getLastStopTime(context) > getMaxInterruptionTime(context)
                && System.currentTimeMillis() - getLastCreateTime(context)> 500; // we dont want to call onNewSession if onCreate has just been called
    }
}
