/*
 * Copyright (C) 2014 Nathan Zylbersztejn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.znat.kick;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.content.res.Resources.NotFoundException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;


public class Utils {

	
	public static int findOriginalIndex(int listIndex, int position){
		return 2 * (position-1) + listIndex;
	}
	
	public static void setTypeFaceRecursively(ViewGroup root, Typeface tf) {
        for(int i = 0; i <root.getChildCount(); i++) {
            View v = root.getChildAt(i);
            if(v instanceof TextView ) {
                    ((TextView)v).setTypeface(tf);
            } else if(v instanceof Button) {
                    ((Button)v).setTypeface(tf);
            } else if(v instanceof EditText) {
                    ((EditText)v).setTypeface(tf);
            } else if(v instanceof ViewGroup) {
                    setTypeFaceRecursively((ViewGroup)v,tf);
            }
        }
    }
	
	public static void underlineTextView(TextView tv){
		tv.setText(Html.fromHtml("<u>" +tv.getText().toString() + "</u>"));
	}

	
	public static float getTextViewMeasuredWidthFromText(TextView tv){
		float textWidth = tv.getPaint().measureText(tv.getText().toString());
		float padding = tv.getPaddingLeft() + tv.getPaddingRight();
		MarginLayoutParams lp = (MarginLayoutParams) tv.getLayoutParams();
		float margins = lp.leftMargin + lp.rightMargin;
		return textWidth + padding + margins;
	}
	
	
	/**
	 * Return storage location
	 * @param context
	 * @return
	 */
	public static File getStorage(Context context){
		if (KickApplication.get().isDebug())
			return context.getExternalFilesDir(null);
		else
			return context.getFilesDir(); 
	}
	
	public static File getCacheDir(Context context){
		if (KickApplication.get().isDebug())
			return context.getExternalCacheDir();
		else
			return context.getCacheDir();	
	}
	
	public static File getNewCacheFile(Context context){
		return new File(getCacheDir(context), String.valueOf(System.currentTimeMillis()));
	}
	
	
	public static void openUrlInBrowser(Context context, String url){
		Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
		context.startActivity(browserIntent);
	}
	
	public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
	        int reqWidth, int reqHeight, boolean transparency) {

	    // First decode with inJustDecodeBounds=true to check dimensions
	    final BitmapFactory.Options options = new BitmapFactory.Options();
	    options.inJustDecodeBounds = true;
	    BitmapFactory.decodeResource(res, resId, options);

	    // Calculate inSampleSize
	    options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

	    // Decode bitmap with inSampleSize set
	    options.inJustDecodeBounds = false;
	    if(!transparency) {
	    	options.inPreferredConfig = Bitmap.Config.RGB_565;
	    }
	    return BitmapFactory.decodeResource(res, resId, options);
	}
	public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
		int inSampleSize = 1;
		final int height = options.outHeight;
		final int width = options.outWidth;
		if (height > reqHeight || width > reqWidth) {
			inSampleSize = Math.max(Math.round((float) height / (float) reqHeight), Math.round((float) width / (float) reqWidth));
		}
		return inSampleSize;
	}

	public static Typeface getFontFromResources(Context context, int resource)
	{ 
	    Typeface tf = null;
	    InputStream is = null;
	    try {
	        is = context.getResources().openRawResource(resource);
	    }
	    catch(NotFoundException e) {
            if (KickApplication.get().isDebug()) e.printStackTrace();
	    }

	    String outPath = context.getCacheDir() + "/tmp" + System.currentTimeMillis() + ".raw";

	    try
	    {
	        byte[] buffer = new byte[is.available()];
	        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(outPath));

	        int l = 0;
	        while((l = is.read(buffer)) > 0)
	            bos.write(buffer, 0, l);

	        bos.close();

	        tf = Typeface.createFromFile(outPath);

	        // clean up
	        new File(outPath).delete();
	    }
	    catch (IOException e)
	    {
            if (KickApplication.get().isDebug()) e.printStackTrace();
	    }
	    return tf;      
	}

	
	public static int[] getBitmapDimensions(Context context,int id){
		InputStream inputStream = context.getResources().openRawResource(id);
		BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
		bitmapOptions.inJustDecodeBounds = true;
		BitmapFactory.decodeStream(inputStream, null, bitmapOptions);
		int imageWidth = bitmapOptions.outWidth;
		int imageHeight = bitmapOptions.outHeight;
		try {
			inputStream.close();
		} catch (IOException e) {
			Log.e("Utils", e.getMessage(), e);
		}
		return new int[]{imageWidth, imageHeight};
	}

    /**
     * Get a drawable resources dimensions
     * @param context
     * @param id
     * @return [width, height] array
     */
    public static int[] getImageResourceDimensions(Context context, int id){
        BitmapFactory.Options dimensions = new BitmapFactory.Options();
        dimensions.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(context.getResources(), id, dimensions);
        return new int[]{dimensions.outWidth,dimensions.outHeight};
    }
	
	public static String readFileAsString(Context context, String filepath) throws IOException {
		StringBuilder buf=new StringBuilder();
	    InputStream json=context.getAssets().open(filepath);
	    BufferedReader in = new BufferedReader(new InputStreamReader(json));
	    String str;

	    while ((str=in.readLine()) != null) {
	      buf.append(str);
	    }

	    in.close();
	    
	    return buf.toString();
	}
	
	public static String readFileAsString(Context context, int resourceId) throws IOException {
		StringBuilder buf=new StringBuilder();
		InputStream json = context.getResources().openRawResource(resourceId);
	    BufferedReader in=
	        new BufferedReader(new InputStreamReader(json));
	    String str;

	    while ((str=in.readLine()) != null) {
	      buf.append(str);
	    }

	    in.close();
	    
	    return buf.toString();
	}
	
	public static int getResourceId(Context context, String resourceName, String resourceType){
		return context.getResources().getIdentifier(resourceName, resourceType, context.getPackageName());
	}
	
}
