/*
 * Copyright (C) 2014 Nathan Zylbersztejn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.znat.kick;

import android.support.v4.app.Fragment;
import android.view.Menu;
import android.view.MenuInflater;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nathan on 2014-04-25.
 */
public class KickFragment extends Fragment{
    private boolean mIsAlive;
    private List<Runnable> mToRunOnResume = new ArrayList<Runnable>();
    private List<Runnable> mToRunOnCreateOptionsMenu = new ArrayList<Runnable>();
    @Override
    public void onPause() {
        super.onPause();
        mIsAlive = false;
    }

    @Override
    public void onResume() {
        super.onResume();
        mIsAlive = true;
        for (Runnable r: mToRunOnResume){
            r.run();
        }
        mToRunOnResume.clear();
    }

    /**
     * Adds a runnable to be executed onResume or immediately if onResume has already been called
     * @param r
     */
    public void runNowOrAfterOnResume(Runnable r) {
        if (isAlive())
            r.run();
        else
            mToRunOnResume.add(r);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        for (Runnable r: mToRunOnCreateOptionsMenu){
            r.run();
        }
        mToRunOnCreateOptionsMenu.clear();
        super.onCreateOptionsMenu(menu, inflater);
    }

    /**
     * Adds a runnable to be executed onCreateOptionsMenu or immediately if onResume has already been called
     * @param r
     */
    public void runNowOrAfterOnCreateOptionsMenu(Runnable r){
        if (isAlive())
            r.run();
        else
            mToRunOnCreateOptionsMenu.add(r);
    }

    public boolean isAlive(){
        return mIsAlive;
    }
}
